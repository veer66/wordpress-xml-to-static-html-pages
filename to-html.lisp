(ql:quickload :xmls)
(ql:quickload :cl-ppcre)
(ql:quickload :dexador)

(defpackage :to-html
  (:use :cl :xmls))

(in-package :to-html)

(defparameter *src-pathname* #p"veer66.wordpress.2024-02-28.000.xml")

(defparameter *src-doc* (parse (with-open-file (f *src-pathname*)
				 (format nil "~{~a~^~%~}"
					 (loop for line = (read-line f nil nil)
					       while line
					       collect line)))))


(defparameter *objects* (node-children (car (node-children *src-doc*))))


(defun post-type (item)
  (loop for child in (node-children item)
	when (equal (node-name child) "post_type")
	  do (return (node-children child))))

(defun post-id (item)
  (loop for child in (node-children item)
	when (equal (node-name child) "post_id")
	  do (return (read-from-string (car (node-children child))))))

(defun title (item)
  (loop for child in (node-children item)
	when (equal (node-name child) "title")
	  do (return (car (node-children child)))))

(defun content (item)
  (loop for child in (node-children item)
	when (and (equal (node-name child) "encoded")
		  (equal (node-ns child) "http://purl.org/rss/1.0/modules/content/"))
	  do (return (format nil "~{~a~^~%~}" (node-children child)))))

(defun post-date (item)
  (loop for child in (node-children item)
	when (equal (node-name child) "post_date")
	  do (return (format nil "~{~a~^~%~}" (node-children child)))))

(defun image-urls ()
  (loop with urls = '()
	for o in *objects*
	when (and (equal "item" (node-name o)) (equal '("post") (post-type o)))
	  do
	     (dolist (url (cl-ppcre:all-matches-as-strings "https?://[^\"]+wordpress.com[^\"]+\.(png|jpg|jpeg)" (content o)))
	       (push url urls))
	finally (return urls)))

(defparameter *image-urls* (image-urls))

(defun download-images ()
  (loop for i from 0
	for url in *image-urls*
	as image-pathname = (merge-pathnames (format nil "www/images/~4,'0x~a" i (car (cl-ppcre:all-matches-as-strings "\.(jpg|jpeg|png)$" url))))
	do
	   (handler-case (with-open-file (fo image-pathname
					     :direction :output
					     :if-exists :supersede
					     :element-type '(unsigned-byte 8))
			   (format t "Download ~a to ~a~%" url image-pathname)
			   (loop for b across (dex:get url)
				 do (write-byte b fo))
			   (format t "Done~%"))
	     (DEXADOR.ERROR:HTTP-REQUEST-NOT-FOUND (c)
	       (declare (ignore c))
	       (format *error-output* "Cannot download ~a~%" url)))))

(defun to-local-image (url)
  (let ((idx (search (list url) *image-urls* :test #'equal)))
    (if idx
	(format nil "images/~4,'0x~a" idx (car (cl-ppcre:all-matches-as-strings "\.(jpg|jpeg|png)$" url)))
	url)))

(defun to-local-image* (&rest matches)
  (to-local-image (car matches)))

(defun dump-posts ()
  (loop for o in *objects*
	       with i = 0
;;	       while (< i 3)
	       when (and (equal "item" (node-name o)) (equal '("post") (post-type o)))
		 do
		    (let* ((content (cl-ppcre:regex-replace-all "https?://[^\"#]+wordpress.com[^\"#]+\.(png|jpg|jpeg)"
								(content o)
							       #'to-local-image*
							       :simple-calls t))
			   (post-id (post-id o))
			   (post-pathname (merge-pathnames (format nil "www/~4,'0x.html" post-id))))
		      (format t "Writing ~a ...~%" post-pathname)
		      (with-open-file (fo post-pathname :direction :output
							:if-exists :supersede)
			(format fo "<!DOCTYPE html>~%")
			(format fo "<html>~%")
			(format fo "<head>~%")
			(princ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" fo)
			(princ "<title>" fo)
			(princ (title o) fo)
			(princ "</title>" fo)
			(terpri fo)
			(format fo "<body>~%")
			(format fo "<p>~%<h1>~a</h1>~%</p>~%" (title o))
			(format fo "<p>~%<h3>original post date: ~a</h3>~%</p>~%" (post-date o))
			(format fo "<p>~%")

			(princ content fo)
			(terpri fo)

			(format fo "</p>~%")
			(format fo "</body>~%")
			(format fo "</html>~%"))

		      (incf i))))


(defun post-metadata ()
  (sort (loop for o in *objects*
	      when (and (equal "item" (node-name o)) (equal '("post") (post-type o)))
		collect
		(let* ((post-id (post-id o))
		       (post-link (format nil "~4,'0x.html" post-id))
		       (title (title o)))
		  (list post-id post-link title)))
	#'>
	:key #'car))

(defun dump-index ()
  (with-open-file (fo #P"www/index.html" :direction :output :if-exists :supersede)
    (format fo "<!DOCTYPE html>~%")
    (format fo "<html>~%")
    (format fo "<head>~%")
    (princ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" fo)
    (princ "<title>" fo)
    (princ "Veer66's WordPress Backup" fo)
    (princ "</title>" fo)
    (terpri fo)
    (format fo "<body>~%")
    (format fo "<p>~%<h1>~a</h1>~%</p>~%" "Veer66's WordPress Backup")
    (format fo "<p>~%")
    (format fo "<ul>~%")
    (dolist (p (post-metadata))
      (format fo "<li><a href=\"~a\">~a</a></li>" (cadr p) (caddr p)))
    (format fo "</ul>~%")
    (format fo "</p>~%")
    (format fo "</body>~%")
    (format fo "</html>~%")))
